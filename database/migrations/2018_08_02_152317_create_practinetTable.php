<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePractinetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('practi_login', function (Blueprint $table) {
            $table->increments('connect_id');
            $table->timestamps();
            $table->rememberToken();

            $table->string('name', 191);
            $table->string('email', 191);
            $table->string('password');

            $table->unique('email');
            $table->unique('remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('practi_login');
    }
}

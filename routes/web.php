<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/test', function () {
    return 'HELLO FELLOW HUMAN! I TOO LIKE TO BROWSE STUFF AND EVERYTHING. HAHAHAHA BEING A HUMAN IS COOL.';
});

$router->post('/credential/show',['middleware' => 'verify-key' ,'CredentialController@show']);
/*$router->post('/credential/store',['middleware' => 'verify-key',
                                   'CredentialController@store']);*/
$router->post('/credential/store','CredentialController@store');
$router->post('/token/show','TokenController@show');
$router->put('/token/update','TokenController@update');

$router->post('/email/show','EmailController@show');

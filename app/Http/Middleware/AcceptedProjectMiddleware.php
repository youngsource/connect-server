<?php

namespace App\Http\Middleware;

use function app;
use App\ProjectPolicy;
use Closure;
use Illuminate\Http\Request;

class AcceptedProjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key = $request->get('connect_key');
        if (!ProjectPolicy::isValidKey($key)) {
            return abort(401);
        }
        app()->make(ProjectPolicy::class, [ 'key' => $key ]);

        return $next($request);
    }
}

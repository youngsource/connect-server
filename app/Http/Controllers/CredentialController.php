<?php
/**
 * Created by PhpStorm.
 * User: arnel
 * Date: 4/08/2018
 * Time: 11:07
 */

namespace App\Http\Controllers;


use App\ProjectPolicy;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CredentialController
 * @package App\Http\Controllers
 */
class CredentialController extends Controller
{
    /** @var Request  */
    private $request;
    /** @var string  */
    private $client;


    /**
     * CredentialController constructor.
     * @param Request $request
     * @param ProjectPolicy $keys
     */
    public function __construct(Request $request)
    {
       // dd("test");
        $this->middleware('verify-key');

        $this->request =$request;
        //dd($request);
        $keys= app()->
        dd($keys);
        $this->client =$keys->getCurrentConsumer();
    }

    /**
     * @param connect-id
     * @return JsonResponse
     */
    public function show():JsonResponse{

        ['connect-id' => $id, 'password' => $password] = $this->request->all();

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE connect_id = '$id' AND password='$password'")[0] ?? abort(404)

        );
    }

    /**
     *
     * @return JsonResponse
     */
    public function store():JsonResponse{
        ['name'=>$name, 'password'=>$password,'mail'=>$mail]=$this->request->all();

        return response()->json(
            app('db')->insert(
                "INSERT INTO $this->client (name, email, password) 
                  VALUES ('$name','$mail','$password')"
            )
        );

    }




}
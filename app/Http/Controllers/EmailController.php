<?php
/**
 * Created by PhpStorm.
 * User: arnel
 * Date: 4/08/2018
 * Time: 11:09
 */

namespace App\Http\Controllers;


use App\ProjectPolicy;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmailController
{

    /** @var Request  */
    private $request;
    /** @var string  */
    private $client;


    /**
     * CredentialController constructor.
     * @param Request $request
     * @param ProjectPolicy $keys
     */
    public function __construct(Request $request, ProjectPolicy $keys)
    {
        $this->middleware('AcceptedProjectMiddleware');
        $this->request =$request;
        $this->client =$keys->getCurrentConsumer();
    }

    /**
     *
     * @return JsonResponse
     */
    public function show():JsonResponse{
        ['mail' => $mail, 'password' => $password] = $this->request->all();

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE mail = '$mail' AND password='$password'")[0] ?? abort(404)

        );
    }
}
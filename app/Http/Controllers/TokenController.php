<?php
/**
 * Created by PhpStorm.
 * User: arnel
 * Date: 4/08/2018
 * Time: 11:08
 */

namespace App\Http\Controllers;


use App\ProjectPolicy;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TokenController
{
    /** @var Request  */
    private $request;
    /** @var string  */
    private $client;


    /**
     * CredentialController constructor.
     * @param Request $request
     * @param ProjectPolicy $keys
     */
    public function __construct(Request $request, ProjectPolicy $keys)
    {
        $this->middleware('AcceptedProjectMiddleware');
        $this->request =$request;
        $this->client =$keys->getCurrentConsumer();
    }

    /**
     * @param connect-id
     * @return JsonResponse
     */
    public function show():JsonResponse{
        ['connect-id' => $id, 'token' => $token] = $this->request->all();

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE connect_id = '$id' AND remember_token='$token'")[0] ?? abort(404)

        );
    }

    /**
     *
     * @return JsonResponse
     */
    public function update():JsonResponse{
        ['connect_id' => $id, 'token' => $token, 'password' => $password] = $this->request->all();

        $serverauth = app('db')->select("SELECT * FROM $this->client WHERE id = $id LIMIT 1")[0] ?? abort(404);

        if (!$password === $serverauth->password) {
            abort(400);
        }

        app('db')->update("UPDATE $this->client SET remember_token = '$token' WHERE  id = $id");
        return response('Remember token updated successfully.');

    }
}
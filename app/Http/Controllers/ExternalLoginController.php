<?php /** @noinspection ALL */

namespace App\Http\Controllers;

use App\ProjectPolicy;
use Illuminate\Http\Request;

class ExternalLoginController extends Controller
{
    /** @var array|string[] */
    public static $acceptedProjects = ['abc' => 'practi_login'];
    /** @var Request */
    private $request;
    /** @var string */
    private $client;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @param ProjectPolicy $keys
     */
    public function __construct(Request $request, ProjectPolicy $keys)
    {
        $this->middleware('verify-key');
        $this->request = $request;
        $this->client = $keys->getCurrentConsumer();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function retrieveById(): \Illuminate\Http\JsonResponse
    {
        $id = $this->request->get('connect_id');

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE connect_id = $id")[0] ?? abort(404)
        );
    }

    public function retrieveByToken()
    {
        ['connect_id' => $id, 'token' => $token] = $this->request->all();

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE id = $id AND remember_token='$token' LIMIT 1")[0] ?? abort(404)
        );
    }

    public function updateRememberToken()
    {
        ['connect_id' => $id, 'token' => $token, 'password' => $password] = $this->request->all();

        $serverauth = app('db')->select("SELECT * FROM $this->client WHERE id = $id LIMIT 1")[0] ?? abort(404);

        if (!$password === $serverauth->password) {
            abort(400);
        }

        app('db')->update("UPDATE $this->client SET remember_token = '$token' WHERE  id = $id");
        return response('Remember token updated successfully.');
    }

    public function retrieveByCredentials()
    {
        ['mail' => $mail, 'password' => $password] = $this->request->all();

        return response()->json(
            app('db')->select("SELECT * FROM $this->client WHERE email = '$mail' AND password='$password'")[0] ?? abort(404)
        );
    }

    public function validateCredentials()
    {
        ['connect_id' => $id, 'email' => $email, 'password' => $password] = $this->request->all();

        $serverauth = app('db')->select("SELECT * FROM $this->client WHERE id = $id LIMIT 1")[0] ?? abort(404);

        return response()->json(
            $password === $serverauth->password && $mail === $serverauth->email
        );

    }

    public function createCredential()
    {
        ['token' => $token, 'name' => $name, 'mail' => $mail, 'password' => $password] = $this->request->all();

        return response()->json(
            app('db')->insert(
                "INSERT INTO $this->client (name, email, password, remember_token) 
                  VALUES ('$name','$mail','$password','$token')"
            )
        );
    }
}

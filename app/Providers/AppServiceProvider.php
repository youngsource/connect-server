<?php

namespace App\Providers;

use App\ProjectPolicy;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->app->singleton(ProjectPolicy::class, ProjectPolicy::class);
    }
}

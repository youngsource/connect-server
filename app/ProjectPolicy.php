<?php

namespace App;

/**
 * Class ProjectPolicy
 * Polices the access to the connect server by managing the keys and respective client consumers.
 * @package App
 */
final class ProjectPolicy
{
    private static $acceptedConsumers = [
        'abc' => 'practi-login',
    ];

    /** @var string */
    private $currentConsumer;

    /**
     * ApplicationKeys constructor.
     *
     * @pre The key must be valid.
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->registerCurrentConsumerKey($key);
    }

    /**
     * Registers the current consumer key by loading the correct consumer mapped to this key.
     *
     * @pre The key must be valid.
     * @param string $key
     */
    private function registerCurrentConsumerKey(string $key): void
    {
        $this->currentConsumer = self::$acceptedConsumers[$key];
    }

    /**
     * Checks to see if the key is valid.
     *
     * @param string $key
     * @return bool
     */
    public static function isValidKey(string $key): bool
    {
        return isset(self::$acceptedConsumers[$key]);
    }

    /**
     * Returns the current consumer of the connect-server.
     *
     * @return string
     */
    public function getCurrentConsumer(): string
    {
        return $this->currentConsumer;
    }
}
